var log = require('./libs/log')(module);
var config = require('./libs/config');
var Lead = require('./models/lead');

var http = require('http');
var https = require('https');

var fs = require('fs');
var privateKey  = fs.readFileSync('sslcert/server.pem', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var express = require('express');
var app = express();

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(config.get('http_port'));
httpsServer.listen(config.get('https_port'));

var mongoose = require('mongoose');
var uri = "mongodb://admin:voladminlov@cluster0-shard-00-00-9lrfw.mongodb.net:27017,cluster0-shard-00-01-9lrfw.mongodb.net:27017,cluster0-shard-00-02-9lrfw.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin";
mongoose.connect(uri, { useMongoClient: true });
mongoose.Promise = global.Promise;

var VK = require('vk-io');
var vk = new VK();

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Запро лидов ВК
app.get('/api/vk/leads', function (req, res) {
  
  log.info('get');
  
  var ids = req.params.ids;
  
  if(!ids){
    return res.status(400).send("Отсутствует ids ВК в запросе.");
  }
  
  /*var lead = 
  {
    'id': 'wall' + peer_id
  };*/
    
  db.collection('leads').find(ids, (err, item) => {
    if (err) {
      res.send({'error':'An error has occurred'});
    } else {
      res.send(item);
    }
  });
  
});

// Добавление лида ВК
app.post('/api/vk/leads', function (req, res) {
  
  var url = req.body.url;
  var peer_id = req.body.peer_id;
  
  if(!url && !peer_id){
    return res.status(400).send("Отсутствует url или peer_id ВК в запросе.");
  }
  
  if (!url)
  {
    url = 'https://vk.com/wall' + peer_id;
  }
  
  var lead = { 
    url: url
  }
  
  vk.parseLink(url)
  .then(
    response => {
      lead.vk = response;
      lead.vk.peer_id = response.peer + '_' + response.id;
      lead.id = response.type + lead.vk.peer_id;
      lead.url = 'https://vk.com/' + lead.id;
      
      var l = new Lead.model(lead);
      l.save(function (err) {
        if (err) {
          log.error(err);
          return res.status(500).send("Ошибка БД: " + err);
        } else {
          return res.json(lead);
        }
      });
    },
    error => {
      return res.status(400).send("Не поддерживаемый url ВК в запросе.");
    }
  );
});
