var mongoose = require('mongoose');

var schema = new mongoose.Schema({
  _id: String,
  url: String,
  vk: {
    type: {type: String},
    peer: String,
    id: String
  }
});

module.exports.model = mongoose.model('Lead', schema);